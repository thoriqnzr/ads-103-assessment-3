#pragma once
#include <iostream>
#include <string>

using namespace std;

class DishNode
{
public:
	int DishID;
	string name;
	DishNode* leftChild;
	DishNode* rightChild;

	DishNode(int dish, string name);
};


