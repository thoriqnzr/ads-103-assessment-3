#include "AVL.h"
#include <fstream>

//using recursion, we keep exploring down, and pass final height values up
int AVL::height(DishNode* node)
{
    int h = 0;
    //helps break recursion cycle when we get to nulls at the bottom of branches
    if (node != NULL)
    {
        int leftH = height(node->leftChild);
        int rightH = height(node->rightChild);

        //max gets biggest of the 2 and discards the smaller
        int maxH = max(leftH, rightH);
        h = maxH + 1;
    }
    return h;
}


int AVL::difference(DishNode* node)
{
    //if empty treee, well its balanced, its 0
    if (node == NULL)
        return 0;

    int leftH = height(node->leftChild);
    int rightH = height(node->rightChild);
    int balanceFactor = leftH - rightH;

    return balanceFactor;
}

DishNode* AVL::RRrotation(DishNode* parent)
{
    DishNode* temp = parent->rightChild;
    parent->rightChild = temp->leftChild;
    temp->leftChild = parent;
    if (displayRotations)
        cout << "RR rotation on " << parent->name << endl;

    return temp;
}

DishNode* AVL::LLrotation(DishNode* parent)
{
    DishNode* temp = parent->leftChild;
    parent->leftChild = temp->rightChild;
    temp->rightChild = parent;
    if (displayRotations)
        cout << "RR rotation on " << parent->name << endl;

    return temp;
}


DishNode* AVL::LRrotation(DishNode* parent)
{
	DishNode* temp = parent->rightChild;
    parent->rightChild = LLrotation(temp);
    if (displayRotations)
        cout << "RL rotation on " << parent->name << endl;
    return RRrotation(parent);
}

DishNode* AVL::RLrotation(DishNode* parent)
{
    DishNode* temp = parent->rightChild;
    parent->rightChild = LLrotation(temp);
    if (displayRotations)
        cout << "RL rotation on " << parent->name << endl;
    return RRrotation(parent);
}

DishNode* AVL::balance(DishNode* parent)
{
    //get balance factor
    int balanceFactor = difference(parent);

    //IF balanceFactor not within -1,0,1, then lets work out what rotations to do
    if (balanceFactor > 1)
    {
        //left branch is heavy, now work out is left or right child heavy
        if (difference(parent->leftChild) > 0)
        {
            //left child unbalanced
            parent = LLrotation(parent);
        }
        else
        {
            //right child unbalanced
            parent = LRrotation(parent);
        }
    }
    else if (balanceFactor < -1)
    {
        //right branch is heavy, but which child
        if (difference(parent->rightChild) > 0)
        {
            //left child heavy
            parent = RLrotation(parent);
        }
        else
        {
            //right child heavy
            parent = RRrotation(parent);
        }
    }


    return parent;
}

DishNode* AVL::insertAVL(DishNode* parent, DishNode* newDish)
{
    //if sub tree empty, this becomes the parent
    if (parent == NULL)
    {
        parent = newDish;
        return parent;
    }

    //parent not null, so we haven't found an empty space to stick new student yet
    //so we need to go down either left or right path
    if (newDish->DishID < newDish->DishID)
    {
        parent->leftChild = insertAVL(parent->leftChild, newDish);
        parent = balance(parent);
    }
    else //assume id >= parent's id
    {
        parent->rightChild = insertAVL(parent->rightChild, newDish);
        parent = balance(parent);
    }
}

void AVL::insert(DishNode* newDish)
{
    cout << "Inserting " << newDish->name << " " << newDish->DishID << endl;
    root = insertAVL(root, newDish);
    cout << endl;
}

void AVL::show(DishNode* p)
{

         //here I'm putting the fstream files 
        ofstream WritingFile;
        WritingFile.open("output-q1-a2.txt");
        // Base Case 
        if (root == NULL)  return;

        // Create an empty queue for level order traversal 
        queue<DishLevelNode> q;

        // Enqueue Root and initialize height 
        q.push(DishLevelNode(root, 0));

        int previousOutputLevel = -1;

        while (q.empty() == false)
        {
            // Print front of queue and remove it from queue 
            DishLevelNode node = q.front();
            if (node.level != previousOutputLevel)
            {
                WritingFile << endl;
                WritingFile << node.level << "- ";
                previousOutputLevel = node.level;
            }
            WritingFile << node.dish->DishID << ":" << node.dish->name << " ";
            q.pop();

            /* Enqueue left child */
            if (node.dish->leftChild != NULL)
                q.push(DishLevelNode(node.dish->leftChild, node.level + 1));

            /*Enqueue right child */
            if (node.dish->rightChild != NULL)
                q.push(DishLevelNode(node.dish->rightChild, node.level + 1));

        }
        WritingFile.close();
}
