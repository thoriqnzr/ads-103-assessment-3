#pragma once
#include "DishNode.h"
#include <queue>

//Binary Search Tree
//root node sort of acting as center
//everything smaller then root, is stored on left branch somewhere
//everything else to the right
//HELPER CLASS FOR OUTPUT
class DishLevelNode {
public:
	DishNode *dish;
	int level;

	//constructor
	DishLevelNode(DishNode *dish, int level)
	{
		this->dish = dish;
		this->level = level;
	}
};

class BinarySearchTree
{
public:
	DishNode* root = NULL;
	virtual void insert(DishNode* newDish);
	DishNode* search(int DishID, bool showSearchPath = false);

	//recursive traversal functions
	void inOrderTraversal(DishNode* current);
	void preOrderTraversal(DishNode* current);
	void postOrderTraversal(DishNode* current);
	void show(DishNode* p);
};

