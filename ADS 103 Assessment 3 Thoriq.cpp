#include <iostream>
#include <string>
#include "BinarySearchTree.h"
#include "DishNode.h"
#include "AVL.h"
#include <fstream>//file reading thingy
#include <vector>//for vectors
#include "BinaryMaxHeap.h"
//Including basic stuff and header file
using namespace std;
//============================================================================================
						//Thoriq Feyrouz Nazar Student ID: A00045022 
                        //ASSESSMENT 3 - PROGRAMMING ASSIGNMENT 2
                        //Base code by Matt :D 
//=============================================================================================

void main()
{
	//--------------------------------------------------------------------------------------------------------------
	                //Question 1
	    /*You are required to create an AVL tree by inserting numbers.Numbers that need to be inserted are
		given in the input file.After each insertion, write the contents of the tree in the output file using
		breadth - first traversal.Print the level and then the values at that level(root is at level 0).In the input
		file first line contains the number of data elements.The second line contains all the data elements
		separated by space.*/
	//---------------------------------------------------------------------------------------------------------------

	cout << "Inserting numbers and performing AVL rotations " << endl;
	cout << "============================================================================== \n";
	ifstream readFile; 
	readFile.open("input-q1a2.txt"); //reading file
	int numberOfNumbers;
	
	readFile >> numberOfNumbers; 
	
	AVL avl1; 
	vector<int> nums;
	for (int i = 1; i <= numberOfNumbers; i++) //If loop
	{
		int temp;
		readFile >> temp;             //inserting avl
		nums.push_back(temp);     
		avl1.insert(new DishNode(temp, " ")); 
	}
	readFile.close(); //close file

	cout << "============================================================================== \n";

	cout << "\nOperation successful! Please check the output file 'output-q1-a2.txt' in the folder!" <<  endl;
	
	avl1.show(avl1.root); //show the avl (starting from root)

//========================================================================================================

    //----------------------------------------------------------------------------------------------------
	              //Question 2
	   /*You are required to create a binary max heap by inserting numbers(you may use arrays or dynamic
		data structure).Numbers that need to be inserted are given in the input file.After each insertion,
		write the contents of the heap, beforeand after the heap operation in the output file.In the input file,
		first line contains the number of data elements.The second line contains all the data elements
		separated by space.*/
	//-----------------------------------------------------------------------------------------------------

	cout << "\n==============================================================================\n";
	ifstream readFile2;
	int numberOfNumbers2;
	readFile2.open("input-q2-a2.txt"); //reading the file
	readFile2 >> numberOfNumbers2;

	
	BinaryMaxHeap Heap1;
	
	vector<int> numbers2;
	for (int i = 1; i <= numberOfNumbers2; i++) //loopyloop
	{
		int temp2;
		readFile2 >> temp2;        //inserting max heap
		Heap1.Insert(DishNode(temp2, " "));
	}

	cout << "\nOperating MAX HEAP! Please check the output file 'output-q2-a2.txt' in the folder!\n" << endl;

	cout << "==============================================================================";

	Heap1.showHeap(); //showing the heap


	system("pause");
}