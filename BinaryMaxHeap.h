#pragma once
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include "DishNode.h"
#pragma once
class BinaryMaxHeap
{
public:
	vector<DishNode> heap;
	int leftChildIndex(int parent);
	int rightChildIndex(int parent);
	int parentIndex(int child);

	void heapifyup(int index);
	void heapifydown(int index);

	void Insert(DishNode element);
	void DeleteMax();
	DishNode* ExtractMax();
	void showHeap();
	int Size();

};

