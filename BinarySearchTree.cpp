#include "BinarySearchTree.h"
#include <fstream>

void BinarySearchTree::insert(DishNode* newDish)
{
    //IF the root is NULL(tree is empty), then make this student the root
    if (root == NULL)
    {
        root = newDish;
        return; //exit function early, we are done here
    }

    //some pointers to help us navigate the tree to find where to put the new student
    DishNode* current = root; //current node we're pointing at
    DishNode* parent = NULL; //parent of current (node visitored last time)

    while (true)//infinite loop
    {
        //lets keep track of where we were before moving down further
        parent = current;
        //LEFT OR RIGHT?!
        //if new students studentID is less then the student at current node, then go down LEFT
        if (newDish->DishID < current->DishID)
        {
            //< means we go down deeper into tree on left side
            current = current->leftChild;
            //if current is NULL, we just found an empty space to insert our new Student :D
            if (current == NULL)
            {
                //done, stick student here
                parent->leftChild = newDish;
                return; //done, bail
            }
        }
        else
        {
            //go down right path
            current = current->rightChild;
            //if current is NULL, insert there
            if (current == NULL)
            {
                parent->rightChild = newDish;
                return;
            }
        }
    }
}

DishNode* BinarySearchTree::search(int DishID, bool showSearchPath)
{

    //if tree empty, cant find student matching studentID then
    if (root == NULL)
    {
        return NULL;
    }

    DishNode* current = root;

    //keep looping until I find a match
    while (current->DishID != DishID)
    {


        if (showSearchPath)
            cout << current->DishID << " " << current->name << endl;

        //havent found it yet, lefts explore left or right down in the tree
        if (DishID < current->DishID)
        {
            //go left
            current = current->leftChild;
        }
        else
        {
            //go right
            current = current->rightChild;
        }

        //if current is NULL, search studentID cannot be found
        if (current == NULL)
        {
            return NULL;
        }

    }

    return current; //should be pointing to match in tree :D
}

void BinarySearchTree::inOrderTraversal(DishNode* current)
{
    //current == null is end of this branch path, this if
   //stops from infinite looping
    if (current != NULL)
    {
        inOrderTraversal(current->leftChild);
        cout << current->DishID << " " << current->name << endl;
        inOrderTraversal(current->rightChild);
    }
}

void BinarySearchTree::preOrderTraversal(DishNode* current)
{
    if (current != NULL)
    {
        cout << current->DishID << " " << current->name << endl;
        preOrderTraversal(current->leftChild);
        preOrderTraversal(current->rightChild);
    }
}

void BinarySearchTree::postOrderTraversal(DishNode* current)
{

    if (current != NULL)
    {
        postOrderTraversal(current->leftChild);
        postOrderTraversal(current->rightChild);
        cout << current->DishID << " " << current->name << endl;
    }
}

//referenced second algorithm here https://www.geeksforgeeks.org/level-order-tree-traversal/
void BinarySearchTree::show(DishNode* p)
{
    // Base Case 
    if (root == NULL)  return;

    // Create an empty queue for level order traversal 
    queue<DishLevelNode> q;

    // Enqueue Root and initialize height 
    q.push(DishLevelNode(root, 0));

    int previousOutputLevel = -1;

    while (q.empty() == false)
    {
        // Print front of queue and remove it from queue 
        DishLevelNode node = q.front();
        if (node.level != previousOutputLevel)
        {
            cout << endl;
            cout << node.level << "- ";
            previousOutputLevel = node.level;
        }
        cout << node.dish->DishID << ":" << node.dish->name << " ";
        q.pop();

        /* Enqueue left child */
        if (node.dish->leftChild != NULL)
            q.push(DishLevelNode(node.dish->leftChild, node.level + 1));

        /*Enqueue right child */
        if (node.dish->rightChild != NULL)
            q.push(DishLevelNode(node.dish->rightChild, node.level + 1));

    }
}
