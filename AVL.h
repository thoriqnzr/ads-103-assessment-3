#include "BinarySearchTree.h"
#pragma once
//AVL is self balancing binary search tree
//AVL - Adelson, Velski & Landis
class AVL : public BinarySearchTree
{
public:
	bool displayRotations = true;

	//works out height of sub tree
	int height(DishNode* node);

	//difference between left and right sub trees
	int difference(DishNode* node);

	//ROTATIONS
	//return: new parent of subtree
	//parameter: current parent of sub tree
	//right branch, right child
	DishNode* RRrotation(DishNode* parent);
	//left branch, left child
	DishNode* LLrotation(DishNode* parent);
	//left branch, right child
	DishNode* LRrotation(DishNode* parent);
	//right branch, left child
	DishNode* RLrotation(DishNode* parent);

	//BALANCE
	//balances a tree structure where parent is the middle top node
	//returns new parent after balancing(rotations)
	DishNode* balance(DishNode* parent);

	//INSERT
	//recursive insert that considers parent a sub tree
	//this insert also balances itself
	//returns the new root node of the tree
	DishNode* insertAVL(DishNode* parent, DishNode* newDish);

	//overriding insert from parent
	void insert(DishNode* newDish);

	void show(DishNode* p);

	//constructing the show avl function
};
